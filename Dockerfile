# FROM gradle:8.5.0-jdk17-graal AS builder
FROM container-registry.oracle.com/graalvm/native-image:17-ol8 AS builder

ARG OTEL_EXPORTER_OTLP_PROTOCOL
ARG OTEL_EXPORTER_OTLP_ENDPOINT

ENV OTEL_EXPORTER_OTLP_PROTOCOL=${OTEL_EXPORTER_OTLP_PROTOCOL:-}
ENV OTEL_EXPORTER_OTLP_ENDPOINT=${OTEL_EXPORTER_OTLP_ENDPOINT:-}

WORKDIR /app

COPY . /app

RUN microdnf install findutils

RUN ./gradlew clean nativeCompile --no-daemon --parallel

# FROM alpine:3.18.5
FROM container-registry.oracle.com/os/oraclelinux:8-slim

WORKDIR /app

COPY --from=builder /app/build/native/nativeCompile/otlpdemo /app

EXPOSE 8080

ENTRYPOINT "/app/otlpdemo"
