package io.softbutterfly.instana.otlpdemo;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import io.opentelemetry.api.OpenTelemetry;
import io.opentelemetry.api.baggage.propagation.W3CBaggagePropagator;
import io.opentelemetry.api.trace.propagation.W3CTraceContextPropagator;
import io.opentelemetry.context.propagation.ContextPropagators;
import io.opentelemetry.context.propagation.TextMapPropagator;
import io.opentelemetry.exporter.otlp.http.logs.OtlpHttpLogRecordExporter;
import io.opentelemetry.exporter.otlp.http.metrics.OtlpHttpMetricExporter;
import io.opentelemetry.exporter.otlp.http.trace.OtlpHttpSpanExporter;
import io.opentelemetry.sdk.OpenTelemetrySdk;
import io.opentelemetry.sdk.logs.SdkLoggerProvider;
import io.opentelemetry.sdk.logs.export.BatchLogRecordProcessor;
import io.opentelemetry.sdk.metrics.SdkMeterProvider;
import io.opentelemetry.sdk.metrics.export.PeriodicMetricReader;
import io.opentelemetry.sdk.resources.Resource;
import io.opentelemetry.sdk.trace.SdkTracerProvider;
import io.opentelemetry.sdk.trace.export.BatchSpanProcessor;
import io.opentelemetry.semconv.ResourceAttributes;

/**
 * Configuration class for initializing OpenTelemetry.
 */
public class OpenTelemetrySettings {

    /**
     * Default constructor.
     */
    public OpenTelemetrySettings() {
    }

    /**
     * Initializes and configures OpenTelemetry.
     *
     * @return Initialized OpenTelemetry instance.
     */
    static OpenTelemetry initOpenTelemetry() {
        // Define resource attributes
        Resource resource = Resource.getDefault()
                .toBuilder()
                .put(ResourceAttributes.SERVICE_NAME, "OtlpDemo")
                .put(ResourceAttributes.SERVICE_VERSION, "0.1.0")
                .build();

        // Configure and build SpanExporter
        OtlpHttpSpanExporter spanExporter = OtlpHttpSpanExporter.builder()
                .setTimeout(2, TimeUnit.SECONDS)
                .build();

        // Configure and build SpanProcessor
        BatchSpanProcessor spanProcessor = BatchSpanProcessor.builder(spanExporter)
                .setScheduleDelay(100, TimeUnit.MILLISECONDS)
                .build();

        // Configure and build TracerProvider
        SdkTracerProvider tracerProvider = SdkTracerProvider.builder()
                .addSpanProcessor(spanProcessor)
                .setResource(resource)
                .build();

        // Configure and build MetricExporter and MetricReader
        OtlpHttpMetricExporter metricExporter = OtlpHttpMetricExporter.getDefault();
        PeriodicMetricReader metricReader = PeriodicMetricReader.builder(metricExporter)
                .setInterval(Duration.ofMillis(1000))
                .build();
        SdkMeterProvider meterProvider = SdkMeterProvider.builder()
                .registerMetricReader(metricReader)
                .setResource(resource)
                .build();

        // Configure and build LogRecordExporter and LogRecordProcessor
        OtlpHttpLogRecordExporter logRecordExporter = OtlpHttpLogRecordExporter.builder().build();
        BatchLogRecordProcessor logRecordProcessor = BatchLogRecordProcessor.builder(logRecordExporter).build();
        SdkLoggerProvider loggerProvider = SdkLoggerProvider.builder()
                .addLogRecordProcessor(logRecordProcessor)
                .setResource(resource)
                .build();

        // Configure propagators
        TextMapPropagator textMapPropagator = TextMapPropagator.composite(
                W3CTraceContextPropagator.getInstance(),
                W3CBaggagePropagator.getInstance());
        ContextPropagators contextPropagators = ContextPropagators.create(textMapPropagator);

        // Build and register the global OpenTelemetry instance
        OpenTelemetrySdk openTelemetry = OpenTelemetrySdk.builder()
                .setTracerProvider(tracerProvider)
                .setMeterProvider(meterProvider)
                .setLoggerProvider(loggerProvider)
                .setPropagators(contextPropagators)
                .buildAndRegisterGlobal();

        return openTelemetry;
    }
}
