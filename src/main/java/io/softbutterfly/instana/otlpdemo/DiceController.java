package io.softbutterfly.instana.otlpdemo;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.beans.factory.annotation.Autowired;

import io.opentelemetry.api.OpenTelemetry;
import io.opentelemetry.api.trace.Tracer;
import io.opentelemetry.api.trace.Span;
import io.opentelemetry.context.Scope;

/**
 * Controller for handling dice-related requests.
 */
@RestController
public class DiceController {

    private static final Logger logger = LoggerFactory.getLogger(DiceController.class);
    private Tracer tracer;

    /**
     * Constructor to inject OpenTelemetry bean.
     *
     * @param openTelemetry OpenTelemetry instance.
     */
    @Autowired
    void RollController(OpenTelemetry openTelemetry) {
        // Get the tracer from the OpenTelemetry instance
        this.tracer = openTelemetry.getTracer("DiceController", "0.1.0");
    }

    /**
     * Handles the "/dice" endpoint for rolling dice.
     *
     * @param player Optional player parameter.
     * @param rolls  Optional rolls parameter.
     * @return List of integers representing dice rolls.
     */
    @GetMapping("/dice")
    public List<Integer> index(@RequestParam("player") Optional<String> player,
            @RequestParam("rolls") Optional<Integer> rolls) {

        // Start a new span for handling the dice rolling operation
        Span span = this.tracer.spanBuilder("DiceCont/dice").startSpan();

        // Make the span the current span
        try (Scope scope = span.makeCurrent()) {

            // Check if the "rolls" parameter is present
            if (!rolls.isPresent()) {
                // Throw a 400 Bad Request exception if "rolls" is missing
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Missing rolls parameter", null);
            }

            // Roll the dice based on the provided number of rolls
            List<Integer> result = new Dice(1, 6).rollTheDice(rolls.get());

            // Log the result, including the player if provided
            if (player.isPresent()) {
                logger.info("{} is rolling the dice: {}", player.get(), result);
            } else {
                logger.info("Anonymous player is rolling the dice: {}", result);
            }

            // Return the result of dice rolls
            return result;
        } catch (Throwable t) {
            // Record an exception in the span and rethrow the exception
            span.recordException(t);
            throw t;
        } finally {
            // End the span after the dice rolling operation
            span.end();
        }
    }
}
