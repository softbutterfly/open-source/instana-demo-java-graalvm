package io.softbutterfly.instana.otlpdemo;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import io.opentelemetry.api.OpenTelemetry;
import io.opentelemetry.api.trace.Tracer;
import io.opentelemetry.api.trace.Span;

/**
 * Represents a dice for rolling operations.
 */
public class Dice {

    private int min;
    private int max;
    private Tracer tracer;

    /**
     * Constructor with OpenTelemetry integration.
     *
     * @param min           Minimum value of the dice.
     * @param max           Maximum value of the dice.
     * @param openTelemetry OpenTelemetry instance for tracing.
     */
    public Dice(int min, int max, OpenTelemetry openTelemetry) {
        this.min = min;
        this.max = max;
        this.tracer = openTelemetry.getTracer(Dice.class.getName(), "0.1.0");
    }

    /**
     * Constructor without OpenTelemetry integration.
     *
     * @param min Minimum value of the dice.
     * @param max Maximum value of the dice.
     */
    public Dice(int min, int max) {
        this(min, max, OpenTelemetry.noop());
    }

    /**
     * Simulates rolling the dice a specified number of times.
     *
     * @param rolls Number of rolls to simulate.
     * @return List of integers representing the results of each roll.
     */
    public List<Integer> rollTheDice(int rolls) {
        // Start a new span for the "rollTheDice" operation
        Span span = tracer.spanBuilder("Dice/rollTheDice").startSpan();
        List<Integer> results = new ArrayList<>();

        try {
            // Roll the dice the specified number of times
            for (int i = 0; i < rolls; i++) {
                results.add(this.rollOnce());
            }

            // Return the list of roll results
            return results;

        } finally {
            // End the span after the rolling operation
            span.end();
        }
    }

    /**
     * Simulates rolling the dice once.
     *
     * @return Random integer within the range of the dice.
     */
    private int rollOnce() {
        // Start a new span for the "rollOnce" operation
        Span span = this.tracer.spanBuilder("Dice/rollOnce").startSpan();

        try {
            // Generate a random integer within the range of the dice
            return ThreadLocalRandom.current().nextInt(this.min, this.max + 1);

        } finally {
            // End the span after the rolling operation
            span.end();
        }
    }
}
