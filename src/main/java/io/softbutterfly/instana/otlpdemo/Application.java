package io.softbutterfly.instana.otlpdemo;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import io.opentelemetry.api.OpenTelemetry;

/**
 * The main class for the OtlpDemo application.
 */
@SpringBootApplication
public class Application {

    /**
     * The main entry point for the OtlpDemo application.
     *
     * @param args Command line arguments.
     */
    public static void main(String[] args) {
        // Create a SpringApplication instance for the Application class
        SpringApplication app = new SpringApplication(Application.class);

        // Disable the banner (Spring Boot startup banner)
        app.setBannerMode(Banner.Mode.OFF);

        // Run the application
        app.run(args);
    }

    /**
     * Bean method to initialize and configure the OpenTelemetry instance.
     *
     * @return Initialized OpenTelemetry instance.
     */
    @Bean
    OpenTelemetry openTelemetry() {
        // Call the initOpenTelemetry method from OpenTelemetrySettings to configure
        // OpenTelemetry
        return OpenTelemetrySettings.initOpenTelemetry();
    }
}
