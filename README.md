![Community-Project](https://gitlab.com/softbutterfly/open-source/open-source-office/-/raw/master/banners/softbutterfly-open-source--banner--community-project.png)

# Aplicación de Ejemplo en Java con GraalVM e Instana

Este es un ejemplo de implementación de instrumentación manual de Open Telemetry para una aplicación Java con Spring Boot, compilada como imagen nativa para GraalVM.

## Requerimientos

Para ejecutar este proyecto, necesitarás las siguientes herramientas:

- [Java 17](https://jdk.java.net/17/)
- [Gradle 8.5](https://gradle.org/)
- [Docker](https://docs.docker.com/engine/install/)
- [Docker Compose](https://docs.docker.com/compose/install/)

También se recomienda el uso de la herramienta [SdkMan](https://sdkman.io/) para la gestión de versiones de Java y Gradle.

> **Acerca de la Compatibilidad**
>
> Este proyecto fue construido usando Spring Boot 3.2.0, que es la versión más reciente de Spring Boot compatible con las librerías de OpenTelemetry (1.30.x) al momento de la creación de esta implementación de referencia.
>
> El desarrollo en este repositorio funciona con versiones de Spring Boot 2.7.x en adelante, pero hay dificultades al compilar con `native-image`.

## Ejecución

1. Descarga el repositorio:

```bash
git clone git@gitlab.com:softbutterfly/open-source/instana-demo-java-graalvm.git
cd instana-demo-java-graalvm
```

2. Copia y configura los archivos `.env`.

> **Importante**
>
> Lee las indicaciones en el archivo `.env.template` y configura adecuadamente.

```bash
cp .env.template .env.instana
cp .env.template .env
```

3. Construye la imagen de Docker para la aplicación:

```bash
docker compose build otlpdemo_app
```

La construcción puede llevar algún tiempo, dependiendo de los recursos disponibles en tu equipo.

4. Levanta los servicios con `docker compose`:

```bash
docker compose up
```

5. Prueba el servicio y verifica tu cuenta de Instana:

```bash
for i in `seq 1 10000`; do
    curl \
        -X GET \
        -H 'Content-Type: application/json; charset=utf-8' \
        http://localhost:8080/dice\?player\=SoftButterfly\&rolls\=4
    echo ""
    sleep 0.1
done;
```

![instana](assets/instana.png)

## Desarrollo

Para la instrumentación de esta aplicación de Spring Boot con Open Telemetry, se ha tomado como referencia la [documentación](https://opentelemetry.io/docs/instrumentation/java/manual/) de OpenTelemetry para la instrumentación manual.

La aplicación replica y pone en funcionamiento la aplicación de muestra que OpenTelemetry provee.

Los puntos importantes a destacar para el correcto funcionamiento de la aplicación son los siguientes:

- Las versiones de las librerías empleadas se especifican en el archivo `build.gradle` y son las siguientes:

   ```groovy
   plugins {
       id 'application'
       id 'java'
       id 'org.springframework.boot' version '3.2.0-SNAPSHOT'
       id 'io.spring.dependency-management' version '1.1.4'
       id 'org.graalvm.buildtools.native' version '0.9.28'
    }

   dependencies {
       implementation 'javax.validation:validation-api:2.0.1.Final '

       implementation 'org.springframework.boot:spring-boot-starter-web'
       implementation 'org.springframework.boot:spring-boot-starter-actuator '

       implementation 'io.opentelemetry:opentelemetry-api:1.30.0'
       implementation 'io.opentelemetry:opentelemetry-sdk:1.30.0'
       implementation 'io.opentelemetry:opentelemetry-sdk-metrics:1.30.0'
       implementation 'io.opentelemetry:opentelemetry-exporter-otlp:1.30.0'
       implementation 'io.opentelemetry:opentelemetry-exporter-otlp-logs:1.26.0-alpha'
       implementation 'io.opentelemetry:opentelemetry-exporter-logging:1.30.0'
       implementation 'io.opentelemetry:opentelemetry-sdk-extension-autoconfigure:1.30.0'
       implementation 'io.opentelemetry:opentelemetry-sdk-extension-autoconfigure-spi:1.30.0'
       implementation 'io.opentelemetry.semconv:opentelemetry-semconv:1.23.1-alpha '

       developmentOnly 'org.springframework.boot:spring-boot-devtools '

       testImplementation 'org.springframework.boot:spring-boot-starter-test'
   }
   ```

   En la documentación de OpenTelemetry no se indica la versión exacta de Spring Boot a emplear, y las versiones de los distintos componentes de OpenTelemetry se indican como `1.32.0`.

- La configuración del módulo de OpenTelemetry se ha colocado en el archivo `OpenTelemetrySettings.java`, donde se espera que se centralicen todas las configuraciones de OpenTelemetry.

- En el archivo `OpenTelemetrySettings.java`, se han configurado los siguientes componentes:

  - **Span Exporter**: Para tener visibilidad de la trazabilidad distribuida con ejemplos de *spans* padres e hijos cuyo contexto se propaga empleando los propagadores `W3CTraceContextPropagator` y `W3CBaggagePropagator`. Los *spans* son procesados mediante el `BatchSpanProcessor`.

  - **Metric Exporter**: Para exportar métricas de rendimiento de la aplicación recopilada a intervalos de 1 segundo.

  - **Log Exporter**: Para la recopilación de logs.

- En el archivo `Application.java`, se declara un *bean* que permite inicializar la instrumentación de OpenTelemetry, que luego será inyectada en los controladores.

  ```java
  @Bean
  OpenTelemetry openTelemetry() {
      return OpenTelemetrySettings.initOpenTelemetry();
  }
  ```

- En el archivo `DiceController.java`, se implementa de forma manual la trazabilidad distribuida del *handler* para la ruta `/dice`. El constructor incluye la creación del *tracer* al recibir la instancia de OpenTelemetry y extraer el *tracer*. En el *handler*, se incluye la definición del *scope* para que llamadas posteriores se aniden dentro del *span* del *handler*.

- La trazabilidad se extiende desde las operaciones en `DiceController.java` hacia las operaciones en el archivo `Dice.java` empleando la misma técnica.
